<?php

namespace Drupal\letsencrypt_challenge\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ChallengeController.
 */
class ChallengeController extends ControllerBase {

  /**
   * The Request Stack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The state key value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new ChallengeController object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The Request stack.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   */
  public function __construct(RequestStack $requestStack, StateInterface $state) {
    $this->requestStack = $requestStack;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('state')
    );
  }

  /**
   * Content.
   *
   * @return string
   *   Return challenge string.
   */
  public function content() {
    $response = new CacheableResponse();
    // Default to minimal cache time.
    $response->setMaxAge(5);
    $challenge = $this->resolveChallenge();

    // Serve challenge string or 404.
    if ($challenge != NULL) {
      $response->setStatusCode(200);
      $response->setContent($challenge);
    }
    else {
      $response->setStatusCode(404);
    }

    return $response;
  }

  /**
   * Challenge.
   *
   * @return string
   *   Return challenge string.
   */
  public function resolveChallenge() {
    $domain = $this->requestStack->getCurrentRequest()->getHost();

    // Domain specific challenge, saved in state.
    $challenge = $this->state->get('letsencrypt_challenge.challenge.' . $domain, NULL);
    if ($challenge != NULL) {
      return $challenge;
    }

    // Site-wide challenge, saved in state.
    $challenge = $this->state->get('letsencrypt_challenge.challenge', NULL);
    if ($challenge != NULL) {
      return $challenge;
    }

    return NULL;
  }

}
